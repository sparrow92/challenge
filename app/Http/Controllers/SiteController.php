<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use Illuminate\Routing\Controller;

class SiteController extends Controller
{
    public function home() {
        return redirect()->route('form');
    }

    public function form() {
        return view('form');
    }

    public function formSend(Request $request) {
        $validated = $request->validate([
            'title' => 'required|max:255',
            'year' => 'required|integer|min:1901|max:2155',
            'type' => 'required|in:movie,series',
            'seasons' => 'nullable|integer',
            'episodes' => 'nullable|integer',
            'duration' => 'nullable|integer',
            'description' => 'nullable|max:65535',
            'poster' => 'nullable|image',
        ], [], [
            'title' => 'tytuł',
            'year' => 'rok',
            'type' => 'typ',
            'seasons' => 'liczba sezonow',
            'episodes' => 'liczba epizodow',
            'duration' => 'czas trwania',
            'description' => 'opis',
            'poster' => 'plakat',
        ]);

        if($request->type == 'movie' && !$request->duration) {
            return redirect()->back()->wihtInput()->withErrors('Film musi mieć podany czas trwania!');
        }
        if($request->type == 'series' && (!$request->episodes || !$request->seasons)) {
            return redirect()->back()->wihtInput()->withErrors('Serial musi mieć podaną ilość sezonów i odcinków!');
        }

        $movie = new Movie();
        $movie->title = $request->title;
        $movie->year = $request->year;
        $movie->type = $request->type;
        if($request->type == 'movie') {
            $movie->duration = $request->duration;
        }
        if($request->type == 'series') {
            $movie->seasons = $request->seasons;
            $movie->episodes = $request->episodes;
        }
        $movie->description = $request->description;
        $movie->poster = $request->poster;

        $file = $request->file('poster');
        if ($file) {
            $name = md5($file->getClientOriginalName().date('Y-m-d.h:i:s'));
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $file_name = sprintf('%s.%s', $name, $extension);

            $file->storeAs(
                'posters',
                $file_name,
                'public_uploads'
            );

            $movie->poster = $file_name;
        }

        $movie->save();

        return redirect()->route('list')->withSuccess('Poprawnie dodano materiał do bazy.');
    }

    public function list() {
        $movies = Movie::get();

        return view('list', compact('movies'));
    }

    public function movie(Movie $movie) {
        return view('movie', compact('movie'));
    }
}
