{{-- Strona konkretnego filmu --}}
@extends('layouts.main')

@section('content')
    <div>
        <h1>
            {{$movie->title}}
        </h1>
        <img src="{{'/uploads/posters/'.$movie->poster}}" alt="Italian Trulli" style="max-width: 150px;">
    </div>
@endsection

@section('scripts') 
    <script>
        console.log('Strona ze szczegółami filmu')
    </script>
@endsection
