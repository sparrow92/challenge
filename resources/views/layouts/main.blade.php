<!DOCTYPE html>
<html lang="pl">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Baza filmów - @yield('title')</title>
      <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
      @yield('styles')
   </head>
   <body>
        @yield('content')

        <script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
        @yield('scripts')
   </body>
</html>