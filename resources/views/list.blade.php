{{-- Lista filmów z filtracją --}}
@extends('layouts.main')

@section('content')

    @foreach ($movies as $key => $movie)
        <div>
            <h3>
                <a href="{{route('movie', $movie)}}">{{$movie->title}}</a>
            </h3>
            <img src="{{'/uploads/posters/'.$movie->poster}}" alt="Italian Trulli" style="max-width: 150px;">
        </div>
        <br /><br />
    @endforeach

@endsection

@section('scripts') 
    <script>
        console.log('Strona z listą filmów')
    </script>
@endsection
