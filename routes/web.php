<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [SiteController::class, 'home'])->name('home');
Route::get('/formularz', [SiteController::class, 'form'])->name('form');
Route::post('/formularz', [SiteController::class, 'formSend'])->name('form.send');

Route::get('/lista', [SiteController::class, 'list'])->name('list');

Route::get('/film/{movie}', [SiteController::class, 'movie'])->name('movie');
